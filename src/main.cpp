#include "frontend.h"

int main(int argc, const char** argv) {
    Frontend f;
    return f.startup(argc, argv);
}
